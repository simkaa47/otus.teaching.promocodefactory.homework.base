﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<List<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task InsertAsync(T value);

        Task<bool> DeleteAsync(Guid id);

        Task<bool> UpdateAsync(T value);


    }
}